import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { CardUsersService } from '../../services/users/card-users.service';

@Component({
  selector: 'app-community',
  templateUrl: './community.component.html',
  styleUrls: ['./community.component.scss']
})
export class CommunityComponent {

  constructor(
    private _PostUser: CardUsersService,
    private sanitizer: DomSanitizer
    ) {
    this.ObtenerPostUser();
   }
  
  getSanitizerUrl (url: string) {
    return this.sanitizer.bypassSecurityTrustUrl(url);

  }
  
  postUser: any;

  ObtenerPostUser() {
    this._PostUser.RequestApi("post",6).subscribe (
      response => {
        this.postUser = response.data;
        console.log(this.postUser);
      },
      error => {
        console.log(error);
      }
    )
  }
}
