import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.scss']
})
export class HeroComponent implements OnInit {
  list = "Esto es una lista";
  persona: any = {
    name: "pepe",
    surname: "liyo"
  }
  constructor() { }

  ngOnInit(): void {
  }

}
