import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CardUsersService {
  UrlBase: string = 'https://dummyapi.io/data/api/';
  appId: string = '609dcf7eabb1222d397f1e7d';

  constructor(
    private _http: HttpClient
  ) { }

  RequestApi (pedido:string, limit:number): Observable<any> {
    let headers = new HttpHeaders().set('app-id', this.appId);

    return this._http.get(this.UrlBase + pedido + "?limit=" + limit, {
      headers: headers
    })
  }
}
